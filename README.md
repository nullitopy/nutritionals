# Nutritionals

RDAs/AIs/ULs assume age &amp; sex (adult male).

Most conversions from volume (e\.g. US cups) to mass (g) are calculated via
public information indirectly sourced through
[aqua-calc\.com](https://www.aqua-calc.com/). Other sources (e\.g.
[fdc\.nal\.usda\.gov](https://fdc.nal.usda.gov/)) are indicated via footnote.

Legend:

- 👎 I don’t think so.
- 🤷 Maybe, but it’s not important.
- 👍 Probably, yeah.

Licensed under the [CC0 1\.0
Universal](https://creativecommons.org/publicdomain/zero/1.0/) license (a
public domain dedication).

## Calcium (Ca)

RDA: 1&#x202f;000 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)

UL: 2&#x202f;500 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)

Supplement this?: 👍

### Sources

| food                                  | mg/serving | serving size (g) | sources                                                                                                                                 |
| :------------------------------------ | ---------: | ---------------: | :-------------------------------------------------------------------------------------------------------------------------------------- |
| tahini                                |     427\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| orange juice (Ca fortified)           |     349\.0 |         \*248\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| soymilk (Ca fortified)                |     299\.0 |         \*243\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| molasses                              |     273\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| tofu (firm; made w/ CaSO<sub>4</sub>) |     253\.0 |   &dagger;126\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| almonds                               |     234\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| collard greens                        |     232\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| tofu (soft; made w/ CaSO<sub>4</sub>) |     138\.0 |           121\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/), [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087) |
| soybeans (cooked)                     |     131\.0 |    &dagger;90\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| spinach (boiled)                      |     123\.0 |    &dagger;90\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| hazelnuts                             |     114\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| beet greens                           |     114\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| turnip greens (boiled)                |      99\.0 |    &dagger;72\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| kale (cooked)                         |      94\.0 |         \*118\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| lentils                               |      79\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| chia seeds                            |      76\.0 |   &ddagger;12\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| bok choy (raw; shredded)              |      74\.0 |          \*70\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| pinto beans (canned)                  |      54\.0 |    &dagger;84\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| chickpeas                             |      53\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| corn tortilla                         |      46\.0 |           ⸸28\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| orange                                |      40\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| bread (whole-wheat)                   |      30\.0 |           ¶32\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| broccoli (raw)                        |      21\.0 |    &dagger;44\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |
| rice (white; long-grain)              |      19\.0 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/?component=1087)                                                                         |
| apple (Golden Delicious)              |      10\.0 |          ※169\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Calcium-HealthProfessional/)                                                                  |

- \*1 US cup.
- &dagger;½ US cups.
- &ddagger;1 US tablespoon.
- ⸸One 6 inch corn tortilla; [see
  here](https://www.eatthismuch.com/food/nutrition/6-corn-tortilla,178866/).
- ¶One slice; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/335240/nutrients).
- ※One medium specimen; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/168202/nutrients).

## Chromium (Cr<sup>3+</sup>)

AI: 35 μg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Chromium-HealthProfessional/)

> In the United States, trivalent chromium (Cr<sup>3+</sup>) ion is considered
> an essential nutrient in humans for insulin, sugar, and lipid metabolism.
> However, in 2014, the European Food Safety Authority, acting for the European
> Union, concluded that there was insufficient evidence for chromium to be
> recognized as essential.
>
> \[…\]
>
> There is disagreement on chromium’s status as an essential nutrient.
> Governmental departments from Australia, New Zealand, India, Japan, and the
> United States consider chromium essential while the European Food Safety
> Authority (EFSA) of the European Union does not.
>
> \[…\]
>
> In 2005, the U\.S. Food and Drug Administration had approved a Qualified
> Health Claim for chromium picolinate with a requirement for very specific
> label wording: “One small study suggests that chromium picolinate may reduce
> the risk of insulin resistance, and therefore possibly may reduce the risk of
> type 2 diabetes. FDA concludes, however, that the existence of such a
> relationship between chromium picolinate and either insulin resistance or
> type 2 diabetes is highly uncertain”. At the same time, in answer to other
> parts of the petition, the FDA rejected claims for chromium picolinate and
> cardiovascular disease, retinopathy, or kidney disease caused by abnormally
> high blood sugar levels. In 2010, chromium picolinate was approved by Health
> Canada to be used in dietary supplements. Approved labeling statements
> include: “a factor in the maintenance of good health”, “provides support for
> healthy glucose metabolism”, “helps the body to metabolize carbohydrates”,
> and “helps the body to metabolize fats”. The European Food Safety Authority
> (EFSA) approved claims in 2010 that chromium contributed to normal
> macronutrient metabolism and maintenance of normal blood glucose
> concentration, but rejected claims for maintenance or achievement of a normal
> body weight, or reduction of tiredness or fatigue.
> [\[2\]](https://en.wikipedia.org/wiki/Chromium)

Supplement this?: 👎

### Sources

> Chromium is present in many foods, including meats, grain products, fruits,
> vegetables, nuts, spices, brewer’s yeast, beer, and wine. However, chromium
> amounts in these foods vary widely depending on local soil and water
> conditions as well as agricultural and manufacturing processes used to
> produce them. For example, the amount of chromium can vary 50-fold in samples
> of oatmeal because of growing and processing differences. Some chromium can
> also be transferred to foods from stainless steel equipment during food
> processing and from pots and pans during cooking.
>
> Most dairy products and foods high in sugar (e\.g. sucrose and fructose) are
> low in chromium.
>
> \[…\]
>
> Determining the chromium content of food is challenging because samples are
> easily contaminated by standard tools used for measurement and analysis.
> [\[1\]](https://ods.od.nih.gov/factsheets/Chromium-HealthProfessional/)

## Iron (Fe)

RDA: 14\.4 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)

Note that RDAs are 1\.8 times larger for vegetarians &amp; vegans than they are
for nonvegetarians:

> This is because heme iron from meat is more bioavailable than nonheme iron
> from plant-based foods, and meat, poultry, and seafood increase the
> absorption of nonheme iron.
> [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)

Also, RDAs are about 2\.25 times larger for premenopausal women than they are
for men and postmenopausal women
[\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/).

Note that _excessive_ intake of iron can possibly lead to iron toxicity due to
an abundance of free iron in the blood
[\[3\]](https://emedicine.medscape.com/article/815213-overview).

UL: 45 mg/day (81 mg/day when adjusting for plant-based diet)
[\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)

Enrichment/fortification programs (e\.g. in the United States) often [enrich
flour and flour-based products](https://en.wikipedia.org/wiki/Enriched_flour)
with iron.

Supplement this?: 👍

### Sources

| food                         | mg/serving | serving size (g) | sources                                                                                                                                            |
| :--------------------------- | ---------: | ---------------: | :------------------------------------------------------------------------------------------------------------------------------------------------- |
| breakfast cereal (fortified) |      18\.0 |          \*35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| white beans (canned)         |       8\.0 |   &dagger;179\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| lentils (boiled)             |       3\.0 |   &ddagger;99\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| spinach (boiled)             |       3\.0 |   &ddagger;90\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| tofu (firm)                  |       3\.0 |  &ddagger;126\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| dark chocolate               |       2\.0 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| kidney beans (canned)        |       2\.0 |  &ddagger;128\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| chickpeas (boiled)           |       2\.0 |   &ddagger;82\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| tomatoes (canned; stewed)    |       2\.0 |  &ddagger;127\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| cashews                      |       2\.0 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| potato (baked; skin intact)  |       1\.9 |          ⸸173\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/), [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/food-details/170093/nutrients) |
| green peas (boiled)          |       1\.0 |   &ddagger;80\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| rice (fortified; parboiled)  |       1\.0 |   &ddagger;79\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| bread                        |       1\.0 |           ¶32\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| raisins                      |       1\.0 |           ※36\.2 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| spaghetti (whole-wheat)      |       1\.0 |   &dagger;140\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| pistachioes                  |       1\.0 |            23\.8 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |
| broccoli (boiled)            |       1\.0 |   &ddagger;78\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Iron-HealthProfessional/)                                                                                |

- \*One “serving”.
- &dagger;1 US cup.
- &ddagger;½ US cups.
- ⸸One medium specimen; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/170093/nutrients).
- ¶One slice; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/335240/nutrients).
- ※¼ US cups.

## Magnesium (Mg)

RDA: 400 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/)

UL (**supplemental only**): 350 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/)

Supplement this?: 👎

### Sources

| food                      | mg/serving | serving size (g) | sources                                                                  |
| :------------------------ | ---------: | ---------------: | :----------------------------------------------------------------------- |
| pumpkin seeds             |        156 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| chia seeds                |        111 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| almonds                   |         80 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| spinach (boiled)          |         78 |          \*90\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| cashews                   |         74 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| peanuts                   |         63 |    &dagger;37\.9 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| shredded wheat            |         61 |   &ddagger;47\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| soymilk                   |         61 |          ⸸243\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| black beans               |         60 |         \*122\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| edamame (podless; cooked) |         50 |          \*85\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| peanut butter (creamy)    |         49 |           ¶32\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| potato (baked; w/ skin)   |         43 |            99\.2 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| rice (brown)              |         42 |         \*101\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| oatmeal (instant)         |         36 |          ※163\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| kidney beans              |         35 |         \*130\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| banana                    |         32 |           ❦80\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| raisins                   |         23 |          \*72\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| bread (whole-wheat)       |         23 |           ⁂32\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| avocado                   |         22 |          \*75\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| broccoli (cooked)         |         12 |          \*78\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| rice (white)              |         10 |         \*102\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| apple                     |          9 |          ☟130\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |
| carrot (raw)              |          7 |           ☟61\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Magnesium-HealthProfessional/) |

#### Footnotes

- \*½ US cups.
- &dagger;¼ US cups.
- &ddagger;Two “large biscuits”; [see
  here](https://fdc.nal.usda.gov/fdc-app.html?utf8=%E2%9C%93&affiliate=usda&query=cereal%2C+shredded+wheat&commit=Search#/food-details/173910/nutrients).
  I believe that this figure is based on Post&trade; Shredded Wheat.
- ⸸1 US cup.
- ¶2 US tablespoons.
- ※One packet; [see
  here](https://fdc.nal.usda.gov/fdc-app.html?utf8=%E2%9C%93&affiliate=usda&query=cereal%2C+shredded+wheat&commit=Search#/food-details/1101601/nutrients).
- ❦One medium banana. Assuming that the peel accounts for &ap;⅓ of the mass.
- ⁂One slice; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/335240/nutrients).
- ☟One medium specimen.

## ω–3 fatty acids (omega–3 fatty acids)

### ALA (α–linolenic acid)

AI: 1\.6 g/day
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/)

Supplement this?: 👎

#### Sources

| food                   | g/serving | serving size (g) | sources                                                                         |
| :--------------------- | --------: | ---------------: | :------------------------------------------------------------------------------ |
| flaxseed oil           |      7\.3 |          \*13\.6 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| chia seeds             |      5\.1 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| walnuts (English)      |      2\.6 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| flaxseeds              |      2\.4 |          \*10\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| canola oil             |      1\.3 |          \*13\.6 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| soybean oil            |      0\.9 |          \*13\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| walnuts (black)        |      0\.8 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| edamame (prepared)     |      0\.3 |    &dagger;77\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| refried beans (canned) |      0\.2 |   &dagger;121\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| kidney beans (canned)  |      0\.1 |   &dagger;128\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |
| baked beans (canned)   |      0\.1 |   &dagger;126\.6 | [\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/) |

- \*1 US tablespoon.
- &dagger;½ US cups.

### EPA (eicosapentaenoic acid)

No adequate intake (AI) values, nor any related values, have been determined
for EPA. EPA is synthesised from ALA in the human liver, but the conversion
rate is low, with studies placing the typical conversion rate at less than 15%
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/)
(although it should be noted that the conversion rate tends to be higher in
women than in men). As a result, although an AI may not be determined, the only
practical way to increase EPA levels is to obtain EPA directly from the diet
and/or from supplementation
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/).

Supplement this?: 👍

#### Sources

Although there are vegan sources of EPA like [<i>Y.
lipolytica</i>](https://en.wikipedia.org/wiki/Yarrowia) and various
[microalgae](https://en.wikipedia.org/wiki/Microalgae), the only sources of EPA
in a typical human diet are from animal sources (primarily [oily
fish](https://en.wikipedia.org/wiki/Oily_fish))
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/)[\[2\]](https://en.wikipedia.org/wiki/Eicosapentaenoic_acid).

### DHA (docosahexaenoic acid)

No adequate intake (AI) values, nor any related values, have been determined
for DHA. DHA is synthesised from ALA in the human liver, but the conversion
rate is low, with studies placing the typical conversion rate at less than 15%
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/).
As a result, although an AI may not be determined, the only practical way to
increase DHA levels is to obtain DHA directly from the diet and/or from
supplementation
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/).

Supplement this?: 👍

#### Sources

Although there are vegan sources of DHA like various
[microalgae](https://en.wikipedia.org/wiki/Microalgae), the only sources of DHA
in a typical human diet are from animal sources (primarily [oily
fish](https://en.wikipedia.org/wiki/Oily_fish))
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/)[\[3\]](https://en.wikipedia.org/wiki/Docosahexaenoic_acid).

### DPA (docosapentaenoic acid)

No adequate intake (AI) values, nor any related values, have been determined
for DPA. DPA is synthesised from ALA in the human liver, but the conversion
rate is low, with studies placing the typical conversion rate at perhaps
6%&#x301c;8% [\[4\]](https://doi.org/10.1002%2Flite.201500013). As a result,
although an AI may not be determined, the only practical way to increase DPA
levels is to obtain DPA directly from the diet and/or from supplementation
[\[1\]](https://ods.od.nih.gov/factsheets/Omega3FattyAcids-HealthProfessional/).

DPA is less commonly discussed than the related DHA and EPA, but DPA
nevertheless seems to have similarly beneficial effects
[\[4\]](https://doi.org/10.1002%2Flite.201500013).

> \[t\]he levels of DPA in human milk are higher than those of EPA and
> comparable to those of DHA, implicating it as potentially important in human
> development.
>
> Further, DPA shares structural similarities with EPA and DHA. As an
> “elongated version of EPA”, DPA has two extra carbons in the chain and the
> same number of double bonds as EPA. Biochemically it is a direct elongation
> product of EPA. The similarity of DPA to EPA and DHA may explain some of its
> overlapping biological functions with these better-studied fatty acids.
> [\[4\]](https://doi.org/10.1002%2Flite.201500013)

#### Sources

Although there are vegan sources of DPA like various
[microalgae](https://en.wikipedia.org/wiki/Microalgae), the only sources of DPA
in a typical human diet are from animal sources (primarily [oily
fish](https://en.wikipedia.org/wiki/Oily_fish))
[\[5\]](https://en.wikipedia.org/wiki/Docosapentaenoic_acid).

## Potassium (K)

AI: 3&#x202f;400 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/)

> Dietary surveys consistently show that people in the United States consume
> less potassium than recommended, which is why the 2015–2020 Dietary
> Guidelines for Americans identifies potassium as a “nutrient of public health
> concern”.
> [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/)

Supplement this?: 👍

> Many dietary supplement manufacturers and distributors limit the amount of
> potassium in their products to 99 mg (which is only about 2% of the DV)
> because of two concerns related to potassium-containing drugs. First, the FDA
> has ruled that some oral drug products that contain potassium chloride and
> provide more than 99 mg potassium are not safe because they have been
> associated with small-bowel lesions. Second, the FDA requires some potassium
> salts containing more than 99 mg potassium per tablet to be labeled with a
> warning about the reports of small-bowel lesions.
> [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/)

Note that the above quotation specifies potassium _chloride_ (KCl); potassium
supplements are also available in potassium bicarbonate (KCHO<sub>3</sub>) and
potassium gluconate (KC<sub>6</sub>H<sub>11</sub>O<sub>7</sub>) forms.

### Sources

| food                      | mg/serving | serving size (g) | sources                                                                  |
| :------------------------ | ---------: | ---------------: | :----------------------------------------------------------------------- |
| apricots (dried)          |        755 |          \*65\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| lentils (cooked)          |        731 |   &dagger;198\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| acorn squash              |        644 |   &dagger;245\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| dried prunes              |        635 |          \*87\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| raisins                   |        618 |          \*72\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| potato (baked; no skin)   |        610 |  &ddagger;156\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| kidney beans (canned)     |        607 |   &dagger;256\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| orange juice              |        496 |   &dagger;248\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| soybeans (boiled)         |        443 |          \*86\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| banana                    |        422 |   &ddagger;80\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| spinach (raw)             |        334 |           ¶60\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| molasses                  |        308 |           ※21\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| tomato (raw)              |        292 |  &ddagger;123\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| soymilk                   |        287 |   &dagger;243\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| broccoli (cooked)         |        229 |          \*78\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| cantaloupe                |        214 |          \*78\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| asparagus (cooked)        |        202 |          \*90\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| apple                     |        195 |  &ddagger;169\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| cashews                   |        187 |            23\.8 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| brown rice (medium-grain) |        154 |   &dagger;195\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| coffee (brewed)           |        116 |   &dagger;237\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| iceberg lettuce           |        102 |    &dagger;72\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| peanut butter             |         90 |           ※16\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| black tea (brewed)        |         88 |   &dagger;236\.8 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| flaxseed (whole)          |         84 |           ※10\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| bread (whole-wheat)       |         81 |           ❦32\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| white rice (medium-grain) |         54 |   &dagger;185\.9 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |
| white bread               |         37 |           ❦32\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Potassium-HealthProfessional/) |

- \*½ US cups.
- &dagger;1 US cup.
- &ddagger;One medium specimen; see
  [here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/170033/nutrients),
  [here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/170457/nutrients),
  and
  [here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/168202/nutrients).
- ⸸One medium banana. Assuming that the peel accounts for &ap;⅓ of the mass.
- ¶2 US cups.
- ※1 US tablespoon.
- ❦One slice; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/335240/nutrients).

## Vitamin A (retinol)

RDA: 900 μg RAE/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminA-HealthProfessional/)

“RAE” stands for **r**etinol **a**ctivity **e**quivalent, and is used to
account for the differing bioequivalencies of various forms of dietary vitamin
A. 1 μg RAE is equivalent to 1 μg of retinol.

UL (**preformed retinol &amp; retinol esters only**): 3&#x202f;000 μg/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminA-HealthProfessional/)

Supplement this?: 🤷

### Sources

| food                         | μg RAE/serving | serving size (g) | sources                                                                 |
| :--------------------------- | -------------: | ---------------: | :---------------------------------------------------------------------- |
| sweet potato (baked in skin) |   1&#x202f;403 |         \*114\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminA-HealthProfessional/) |
| spinach (boiled)             |            573 |    &dagger;90\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminA-HealthProfessional/) |
| pumpkin pie                  |            488 |  &ddagger;133\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminA-HealthProfessional/) |
| carrots (raw)                |            459 |    &dagger;55\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminA-HealthProfessional/) |

- \*One whole sweet potato; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/170134/nutrients).
- &dagger;½ US cups.
- &ddagger;One “piece”; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/172787/nutrients).

## Vitamin B<sub>2</sub> (riboflavin)

RDA: 1\.3 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/)

> In people who eat meat and dairy products, these foods contribute a
> substantial proportion of riboflavin in the diet. For this reason, people who
> live in developing countries and have limited intakes of meat and dairy
> products have an increased risk of riboflavin deficiency. Vegans and those
> who consume little milk in developed countries are also at risk of riboflavin
> inadequacy.
> [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/)

Supplement this?: 👍

### Sources

| food                                | mg/serving | serving size (g) | sources                                                                   |
| :---------------------------------- | ---------: | ---------------: | :------------------------------------------------------------------------ |
| breakfast cereal (fortified)        |       1\.3 |          \*35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| oats (instant; fortified)           |       1\.1 |   &dagger;234\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| almonds                             |       0\.3 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| portobello mushroom (grilled)       |       0\.2 |   &ddagger;60\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| quinoa                              |       0\.2 |   &dagger;184\.8 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| plain bagel (fortified)             |       0\.2 |          ⸸105\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| spinach (raw)                       |       0\.1 |    &dagger;30\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| apple                               |       0\.1 |          ¶223\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| kidney beans (canned)               |       0\.1 |   &dagger;256\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| pasta (elbow macaroni; whole-wheat) |       0\.1 |   &dagger;120\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| bread (whole-wheat)                 |       0\.1 |           ※32\.1 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| sunflower seeds (toasted)           |       0\.1 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| tomatoes (crushed; canned)          |       0\.1 |  &ddagger;121\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |
| white rice (fortified; long-grain)  |       0\.1 |   &ddagger;79\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Riboflavin-HealthProfessional/) |

- \*One “serving”.
- &dagger;1 US cup.
- &ddagger;½ US cups.
- ⸸One medium bagel (diameter of 3\.5&#x301c;4\.0 inches); [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/175049/nutrients).
- ¶One large apple; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/171688/nutrients).
- ※One slice; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/335240/nutrients).

## Vitamin B<sub>6</sub>

RDA: 1\.3 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)

> High intakes of vitamin B<sub>6</sub> from food sources have not been
> reported to cause adverse effects. However, chronic administration of 1–6 g
> oral pyridoxine per day for 12–40 months can cause severe and progressive
> sensory neuropathy characterized by ataxia (loss of control of bodily
> movements). Symptom severity appears to be dose dependent, and the symptoms
> usually stop if the patient discontinues the pyridoxine supplements as soon
> as the neurologic symptoms appear. Other effects of excessive vitamin
> B<sub>6</sub> intakes include painful, disfiguring dermatological lesions;
> photosensitivity; and gastrointestinal symptoms, such as nausea and
> heartburn.
> [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)

UL: 100 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)

Supplement this?: 🤷

### Sources

| food                                  | mg/serving | serving size (g) | sources                                                                                                                |
| :------------------------------------ | ---------: | ---------------: | :--------------------------------------------------------------------------------------------------------------------- |
| chickpeas (canned)                    |       1\.1 |         \*240\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| breakfast cereal (fortified)          |       0\.4 |    &dagger;35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| potatoes (boiled)                     |       0\.4 |         \*156\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| banana                                |       0\.4 |   &ddagger;80\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| spaghetti sauce                       |       0\.4 |         \*257\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| peanuts                               |       0\.3 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| bell pepper (red)                     |       0\.3 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| sweet potato (baked)                  |       0\.3 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| waffle (plain; toasted)               |       0\.3 |           ⸸33\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| shiitake mushroom                     |       0\.3 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| avocado                               |       0\.2 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| bulgur (cooked)                       |       0\.2 |         \*182\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| winter squash (baked)                 |       0\.2 |          ¶102\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| spinach (boiled)                      |       0\.2 |           100\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/), [\[2\]](https://fdc.nal.usda.gov/index.html) |
| bread (whole-wheat)                   |       0\.2 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| white rice (fortified; long-grain)    |       0\.1 |         \*158\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| mixed nuts                            |       0\.1 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| raisins                               |       0\.1 |           ¶72\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| onions                                |       0\.1 |           ¶63\.8 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| tofu (firm; made w/ CaSO<sub>4</sub>) |       0\.1 |          ¶126\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| watermelon                            |       0\.1 |         \*152\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB6-HealthProfessional/)                                               |
| corn grits                            |       0\.1 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| almonds                               |       0\.1 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| bread (white)                         |       0\.1 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| baked beans                           |       0\.1 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |
| green beans                           |       0\.1 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/index.html)                                                                           |

- \*1 US cup.
- &dagger;One “serving”.
- &ddagger;One medium banana. Assuming that the peel accounts for &ap;⅓ of the
  mass.
- ⸸One waffle; [see
  here](https://fdc.nal.usda.gov/fdc-app.html#/food-details/175048/nutrients).
- ¶½ US cups.

## Vitamin B<sub>7</sub> (biotin)

AI: 30 μg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/)

Supplement this?: 👎

### Sources

| food                      | μg/serving | serving size (g) | sources                                                                                                                      |
| :------------------------ | ---------: | ---------------: | :--------------------------------------------------------------------------------------------------------------------------- |
| peanuts                   |       6\.0 |            34\.3 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |
| sunflower seeds (roasted) |       2\.6 |          \*35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/)                                                        |
| sweet potato (cooked)     |       2\.4 |           160\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/), [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015) |
| almonds                   |       1\.5 |          \*34\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/)                                                        |
| strawberry                |       0\.7 |            46\.7 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |
| avocado                   |       0\.5 |            50\.0 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |
| spinach (boiled)          |       0\.5 |    &dagger;90\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/)                                                        |
| broccoli (raw)            |       0\.4 |            44\.4 | [\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/), [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015) |
| tomato                    |       0\.3 |            42\.9 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |
| french fries              |       0\.3 |           100\.0 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |
| oatmeal                   |       0\.2 |           200\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/), [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015) |
| banana                    |       0\.2 |    &dagger;75\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Biotin-HealthProfessional/)                                                        |
| bread                     |       0\.1 |           100\.0 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |
| corn flakes               |       0\.1 |           100\.0 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |
| potato (mashed)           |       0\.1 |           100\.0 | [\[2\]](https://doi.org/10.1016%2Fj.jfca.2003.09.015)                                                                        |

- \*¼ US cups.
- &dagger;½ US cups.

## Vitamin B<sub>12</sub> (cobalamin)

RDA: 2\.4 μg/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminB12-HealthProfessional/)

Supplement this?: 👍

### Sources

| food                         |        μg/serving | serving size (g) | sources                                                                       |
| :--------------------------- | ----------------: | ---------------: | :---------------------------------------------------------------------------- |
| nooch (fortified)            | 8\.3&#x301c;24\.0 |          \*42\.7 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB12-HealthProfessional/)     |
| breakfast cereal (fortified) |              0\.6 |    &dagger;35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB12-HealthProfessional/)     |
| tempeh                       |              0\.1 |   &ddagger;83\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminB12-HealthProfessional/)     |
| miso                         |              0\.1 |           100\.0 | [\[2\]](https://fdc.nal.usda.gov/fdc-app.html#/food-details/172442/nutrients) |

- \*¼ US cups.
- &dagger;One “serving”.
- &ddagger;½ US cups.

## Vitamin C (ascorbic acid)

RDA: 90 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminC-HealthProfessional/)

> Research on vitamin C in the common cold has been divided into effects on
> prevention, duration, and severity. A Cochrane review which looked at at
> least 200 mg/day concluded that vitamin C taken on a regular basis was not
> effective in prevention of the common cold. Restricting analysis to trials
> that used at least 1&#x202f;000 mg/day also saw no prevention benefit.
> However, taking vitamin C on a regular basis did reduce the average duration
> by 8% in adults and 14% in children, and also reduced severity of colds.
>
> The \[vitamin C\] megadosing theory is to a large degree discredited. Modest
> benefits are demonstrated for the common cold. Benefits are not superior when
> supplement intakes of more than 1&#x202f;000 mg/day are compared to intakes
> between 200 and 1&#x202f;000 mg/day, and so not limited to the megadose
> range. [\[3\]](https://en.wikipedia.org/wiki/Vitamin_C)

Supplement this?: 👎

### Sources

| food               | mg/serving | serving size (g) | sources                                                                      |
| :----------------- | ---------: | ---------------: | :--------------------------------------------------------------------------- |
| rose hip           |        426 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| guava              |        228 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| blackcurrant       |        200 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| yellow bell pepper |        183 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| red bell pepper    |        128 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| kale               |        120 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| broccoli           |         90 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| kiwi               |         90 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| green bell pepper  |         80 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| brussels sprouts   |         80 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| loganberry         |         80 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| redcurrant         |         80 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| cloudberry         |         60 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| elderberry         |         60 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| strawberry         |         60 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| papaya             |         60 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| orange             |         53 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| lemon              |         53 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| cauliflower        |         48 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| pineapple          |         48 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| cantaloupe         |         40 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| passionfruit       |         30 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| raspberry          |         30 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| grapefruit         |         30 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| lime               |         30 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| cabbage            |         30 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| spinach            |         30 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| mango              |         28 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| blackberry         |         21 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| cassava            |         21 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| potato             |         20 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| honeydew melon     |         20 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| tomato             |         14 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| cranberry          |         13 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| blueberry          |         10 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| grape              |         10 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| apricot            |         10 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| plum               |         10 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| watermelon         |         10 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| avocado            |          9 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| onion              |          7 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| cherry             |          7 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| peach              |          7 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| apple              |          6 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| carrot             |          6 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |
| asparagus          |          6 |           100\.0 | [\[2\]](https://www.nal.usda.gov/sites/www.nal.usda.gov/files/vitamin_c.pdf) |

## Vitamin D (calciferol)

RDA: 15 μg/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)

Vitamin D comes in two forms: [vitamin D<sub>2</sub>
(ergocalciferol)](https://en.wikipedia.org/wiki/Ergocalciferol) and [vitamin
D<sub>3</sub>
(cholecalciferol)](https://en.wikipedia.org/wiki/Cholecalciferol). Both are
metabolised into [1,25-dihydroxyvitamin
D](https://en.wikipedia.org/wiki/Calcitriol), which is the biologically active
form
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)[\[2\]](https://en.wikipedia.org/wiki/Vitamin_D).
Although vitamins D<sub>2</sub> and D<sub>3</sub> differ in their effective
bioequivalence (as measured by serum
[25(OH)D](https://en.wikipedia.org/wiki/Calcifediol) concentrations), the
difference is not _terribly_ large; a 2016 study
[\[3\]](https://doi.org/10.1210%2Fjc.2016-1871) found that the ratio of free
25(OH)D concentration increases due to D<sub>3</sub>&#x29f8;D<sub>2</sub> was
about 1\.72&#x29f8;1. It’s unclear to me whether or not this difference in
bioequivalence makes D<sub>3</sub> “superior” _in general_; although
D<sub>3</sub> is clearly more efficacious at increasing in-serum free 25(OH)D
levels **given a constant dosage**, it’s less clear how easily this disparity
can be made up for by simply ingesting more D<sub>2</sub>. You can find various
claims out there that the larger bioequivalence of D<sub>3</sub> makes it
clearly superior to D<sub>2</sub>, but the scientific evidence doesn’t seem to
indicate that we should just throw out D<sub>2</sub> entirely. A 2006 essay
[\[4\]](https://doi.org/10.1093/ajcn/84.4.694) concludes that:

> \[…\] the public expects to derive the equivalent effect per unit dose of
> vitamin D, whether it is vitamin D<sub>2</sub> or vitamin D<sub>3</sub>. The
> scientific community is aware that these molecules are not equivalent.
> Therefore, vitamin D<sub>2</sub> should no longer be regarded as a nutrient
> appropriate for supplementation or fortification of foods.

This final sentence starts with “Therefore, \[…\]”, but there’s nothing about
the aforegoing paper that seriously supports the claim that “D<sub>2</sub>
should no longer be regarded as a nutrient appropriate for supplementation or
fortification”. The conclusion that _does_ seem reasonable here, given the
aforegoing paper, is: D<sub>2</sub> should no longer be regarded as
[mole]-for-mole equivalent to D<sub>3</sub> — and furthermore, D<sub>2</sub>
appears to have a shorter biological half-life, and higher lability (more
likely to break down more rapidly when in storage).

Because of the increased biological half-life and decreased lability, **I
expect D<sub>3</sub> (in the diet and/or in supplements) to be easier and more
practically “reliable” to use**. Futhermore, it seems that D<sub>2</sub> intake
can interfere with D<sub>3</sub> (e\.g. D<sub>3</sub> from sunlight) metabolism
[\[5\]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6797055/). However, this
**does not necessarily mean** — as far as I can tell — that the same
biologically active effects cannot be achieved by simply taking a sufficiently
large dose of D<sub>2</sub> regularly (e\.g. once per day). Indeed, a 2012
meta-analysis [\[6\]](https://doi.org/10.3945%2Fajcn.111.031070) found that
although D<sub>3</sub> was statistically significantly superior at raising
serum 25(OH)D levels, the effect was only there with bolus dosing, and the
effect was lost with daily doses.

---

> Excess amounts of vitamin D are toxic.
> [\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)

UL: 100 μg/day
[\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)

---

> Most people in the world meet at least some of their vitamin D needs through
> exposure to sunlight.
> [\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)

Supplement this?: 🧛👍

### Sources

| food                                |       μg/serving | serving size (g) | sources                                                                                                               |
| :---------------------------------- | ---------------: | ---------------: | :-------------------------------------------------------------------------------------------------------------------- |
| cremini mushroom (UV irradiated)    |            13\.1 |            41\.0 | [\[7\]](https://fdc.nal.usda.gov/index.html)                                                                          |
| white mushrooms (UV irradiated)     |             9\.2 |          \*35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)                                               |
| portobello mushroom (UV irradiated) |             4\.7 |            43\.0 | [\[7\]](https://fdc.nal.usda.gov/index.html)                                                                          |
| soy/almond/oat milk (fortified)     | 2\.5&#x301c;3\.6 |   &dagger;240\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)                                               |
| breakfast cereal (fortified)        |             2\.0 |   &ddagger;35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/)                                               |
| portobello mushroom                 |             0\.1 |            43\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/), [\[7\]](https://fdc.nal.usda.gov/index.html) |

- \*½ US cups.
- &dagger;1 US cup.
- &ddagger;One “serving”.

[mole]: https://en.wikipedia.org/wiki/Mole_(unit)

## Zinc (Zn)

RDA: 11 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/)

> Zinc toxicity can occur in both acute and chronic forms. Acute adverse
> effects of high zinc intake include nausea, vomiting, loss of appetite,
> abdominal cramps, diarrhea, and headaches. \[…\] Intakes of 150–450 mg of
> zinc per day have been associated with such chronic effects as low copper
> status, altered iron function, reduced immune function, and reduced levels of
> high-density lipoproteins.
> [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/)

UL: 40 mg/day
[\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/)

[Phytate](https://en.wikipedia.org/wiki/Phytic_acid) is a chelator of zinc; as
a result, high phytate levels in the diet cause zinc to be poorly absorbed in
the body [\[2\]](https://doi.org/10.1136%2Fbmj.326.7386.409). Because most
plant-based food sources are also good sources of phytate
[\[3\]](https://en.wikipedia.org/wiki/Phytic_acid#Food_science), vegans may
require more than the usual RDA of zinc
[\[4\]](https://doi.org/10.1053%2Fjada.2003.50142).

Supplement this?: 👍

### Sources

| food                         | mg/serving | serving size (g) | sources                                                             |
| :--------------------------- | ---------: | ---------------: | :------------------------------------------------------------------ |
| baked beans (canned)         |       2\.9 |         \*126\.6 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| breakfast cereal (fortified) |       2\.8 |    &dagger;35\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| pumpkin seeds                |       2\.2 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| cashews                      |       1\.6 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| chickpeas (cooked)           |       1\.3 |          \*82\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| oatmeal (instant; plain)     |       1\.1 |  &ddagger;163\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| almonds                      |       0\.9 |            28\.3 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| kidney beans (cooked)        |       0\.9 |          \*88\.5 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |
| green peas (cooked)          |       0\.5 |          \*80\.0 | [\[1\]](https://ods.od.nih.gov/factsheets/Zinc-HealthProfessional/) |

- \*½ US cups.
- &dagger;One “serving”.
- &ddagger;One packet; [see
  here](https://fdc.nal.usda.gov/fdc-app.html?utf8=%E2%9C%93&affiliate=usda&query=cereal%2C+shredded+wheat&commit=Search#/food-details/1101601/nutrients).
